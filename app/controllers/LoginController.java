package controllers;

import com.google.inject.Inject;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.*;

public class LoginController extends Controller{

    @Inject
    private FormFactory formFactory;

    //Deze functie wordt aangeroepen als je de website opent (dit is geconfigureerd in conf/routes)
    public Result index() {
        return ok(login.render());
    }

    public Result checkLogin() {
        // Hier worden de ingevoerde inloggegevens opgehaald uit het post request
        DynamicForm requestData = formFactory.form().bindFromRequest();
        String username = requestData.get("username");
        String password = requestData.get("password");

        // Hier komt de code om alle geregistreerde gebruikers uit de database op te halen (als dit niet lukt kan je op
        // ebean select statement play framework zoeken)
        //-------------------------------------------------------------------------------




        //-------------------------------------------------------------------------------



        // for (User u : users){
            // Hier komt de code om te controleren of de logingegevens kloppen en de gebruiker toe te voegen aan de
            // sessie en vervolgens de gebruiker door te linken naar de homepage
        // }
        // Als de gegevens niet kloppen wordt de gebruiker teruggestuurd naar de loginpage
        return ok(login.render());
    }

    public Result registerUser() {
        // Hier moet de code komen om de gebruiker door te sturen naar de registerpage
        return null;
    }
}
